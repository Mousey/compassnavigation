package com.denialmc.compassnavigation;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Utils {
    
	public static int limitNumber(int value, int limit) {
		return value > limit ? limit : value;
	}
	
    public static String replaceModifiers(Player player, String string) {
    	Location location = player.getLocation();
    	return string.replace("<name>", player.getName()).replace("<displayname>", player.getDisplayName()).replace("<x>", Integer.toString(location.getBlockX())).replace("<y>", Integer.toString(location.getBlockY())).replace("<z>", Integer.toString(location.getBlockZ())).replace("<yaw>", Integer.toString((int) location.getYaw())).replace("<pitch>", Integer.toString((int) location.getPitch()));
    }
    
    public static String replaceUnicode(String string) {
    	return string.replace("<3", "\u2764").replace("[*]", "\u2605").replace("[**]", "\u2739").replace("[p]", "\u25CF").replace("[v]", "\u2714").replace("[+]", "\u25C6").replace("[++]", "\u2726").replace("[/]", "\u2588").replace("[cross]", "\u2720").replace("[arrow_right]", "\u27A1").replace("[arrow_down]", "\u2B07").replace("[arrow_left]", "\u2B05").replace("[arrow_up]", "\u2B06");
    }
    
    public static Inventory cloneInventory(String player, Inventory inventory) {
    	Inventory cloned = Bukkit.getServer().createInventory(null, inventory.getSize(), inventory.getTitle().replace("<player>", player));
    	cloned.setContents(inventory.getContents());
    	return cloned;
    }
    
    public static List<String> translateList(List<String> list) {
    	List<String> translated = new ArrayList<String>();
    	for (String string : list) {
    		translated.add(ChatColor.translateAlternateColorCodes('&', replaceUnicode(string)));
    	}
    	return translated;
    }
    
    public static ItemStack handleItem(ConfigurationSection section) {
    	try {
    		Material material = Material.getMaterial(section.getString("material", "AIR"));
	    	if (material != null) {
			   	String name = section.getString("name");
	    		List<String> lore = Utils.translateList(section.getStringList("lore"));
			   	boolean enchanted = section.getBoolean("enchanted", false);
	    		int amount = section.getInt("amount", 1);
	    		short damage = (short) limitNumber(section.getInt("damage"), Short.MAX_VALUE);
	    		
	    		ItemStack item = new ItemStack(material, amount, damage);
	    		ItemMeta meta = item.getItemMeta();
			   	
			   	if (name != null) {
			   		meta.setDisplayName(ChatColor.translateAlternateColorCodes('&', Utils.replaceUnicode(ChatColor.RESET + name)));
			   	}
			   	
			   	if (!lore.isEmpty()) {
			   		meta.setLore(lore);
			   	}
			   	
			   	item.setItemMeta(meta);
				
				if (enchanted) {
					item.addUnsafeEnchantment(Enchantment.WATER_WORKER, 4);
				}
				
				return item;
	    	}
    	} catch (Exception e) {
    		e.printStackTrace();
   		}
    	return null;
    }
}