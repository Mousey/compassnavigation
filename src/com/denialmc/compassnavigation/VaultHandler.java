package com.denialmc.compassnavigation;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.plugin.RegisteredServiceProvider;

public class VaultHandler {
	
	private Economy economy;
	
	public VaultHandler() {
		CompassNavigation plugin = CompassNavigation.getInstance();
        RegisteredServiceProvider<Economy> service = plugin.getServer().getServicesManager().getRegistration(Economy.class);
        if (service != null) {
        	economy = service.getProvider();
        	plugin.getLogger().info("Hooked into Vault for economy!");
        }
	}
	
	public boolean hasEconomy() {
		return economy != null;
	}
	
	public Economy getEconomy() {
		return economy;
	}
}