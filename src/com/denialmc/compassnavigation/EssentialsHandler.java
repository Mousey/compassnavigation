package com.denialmc.compassnavigation;

import org.bukkit.Location;
import org.bukkit.plugin.Plugin;

import com.earth2me.essentials.IEssentials;
import com.earth2me.essentials.api.IWarps;

public class EssentialsHandler {

	private IWarps warps;
	
	public EssentialsHandler() {
		CompassNavigation plugin = CompassNavigation.getInstance();
		Plugin essentials = plugin.getServer().getPluginManager().getPlugin("Essentials");
		if (essentials instanceof IEssentials) {
			IEssentials hook = (IEssentials) essentials;
			warps = hook.getWarps();
			plugin.getLogger().info("Hooked into Essentials for warp teleportation!");
		}
	}
	
	public boolean haveWarps() {
		return warps != null;
	}
	
	public IWarps getWarps() {
		return warps;
	}
	
	public Location getWarp(String warp) {
		if (haveWarps()) {
			try {
				return getWarps().getWarp(warp);
			} catch (Exception e) {
				CompassNavigation.getInstance().getLogger().warning("Couldn't get location for warp '" + warp + "'!");
			}
		}
		return null;
	}
}