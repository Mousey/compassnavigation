package com.denialmc.compassnavigation;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionDefault;
import org.bukkit.plugin.java.JavaPlugin;

import com.denialmc.compassnavigation.AutoUpdater.UpdateResult;

public class CompassNavigation extends JavaPlugin implements Listener {
	
	private static CompassNavigation instance;
	public WorldGuardHandler worldGuardHandler;
	public ProtocolLibHandler protocolLibHandler;
	public VaultHandler vaultHandler;
	public AutoUpdater autoUpdater;
	public EssentialsHandler essentialsHandler;
	public LilypadHandler lilypadHandler;
	
	public HashMap<String, Inventory> inventories = new HashMap<String, Inventory>();
	public HashMap<String, String> sessions = new HashMap<String, String>();
	public HashMap<String, WarmupTimer> timers = new HashMap<String, WarmupTimer>();
	
	public void onEnable() {
		instance = this;
		
		getConfig().options().copyDefaults(true);
		saveConfig();
        
        if (getServer().getPluginManager().isPluginEnabled("ProtocolLib")) {
        	protocolLibHandler = new ProtocolLibHandler(this);
        	getLogger().info("Hooked into ProtocolLib for enchantments and attribute removing!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("Essentials")) {
        	essentialsHandler = new EssentialsHandler();
        }
        
        if (getServer().getPluginManager().isPluginEnabled("WorldGuard")) {
        	worldGuardHandler = new WorldGuardHandler(this);
        	getLogger().info("Hooked into WorldGuard for region compass usage flag!");
        }
        
        if (getServer().getPluginManager().isPluginEnabled("Vault")) {
        	vaultHandler = new VaultHandler();
        }
        
        if (getServer().getPluginManager().isPluginEnabled("LilyPad-Connect")) {
        	lilypadHandler = new LilypadHandler();
        }
        
        if (!getDescription().getVersion().contains("SNAPSHOT") && getConfig().getBoolean("settings.autoUpdate")) {
        	autoUpdater = new AutoUpdater(this, getFile(), true);
        }
        
        loadInventories();
        
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		getServer().getPluginManager().registerEvents(this, this);
	}
	
	public static CompassNavigation getInstance() {
		return instance;
	}
	
	public boolean hasEconomy() {
		return vaultHandler != null && vaultHandler.hasEconomy();
	}
	
	public boolean hasConnect() {
		return lilypadHandler != null && lilypadHandler.hasConnect();
	}
	
	public boolean haveWarps() {
		return essentialsHandler != null && essentialsHandler.haveWarps();
	}
	
	public void loadInventories() {
		inventories.clear();
		
		getLogger().info("Loading inventories...");
		for (String name : getConfig().getConfigurationSection("settings").getKeys(false)) {
			if (!name.startsWith("MemorySection")) {
				if (getConfig().getString("settings." + name + ".title") != null) {
					try {
						Inventory inventory = getServer().createInventory(null, getConfig().getInt("settings." + name + ".rows") * 9, Utils.replaceUnicode(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings." + name + ".title"))));
						
						for (int slot = 0; slot < (inventory.getSize() + 1); slot++) {
							if (getConfig().contains("settings." + name + "." + slot + ".material")) {
								ItemStack item = Utils.handleItem(getConfig().getConfigurationSection("settings." + name + "." + slot));
								if (item != null) {
									inventory.setItem(slot - 1, item);
								} else {
									getLogger().warning("Couldn't set item '" + slot + "' in inventory '" + inventory + "' This is because of a wrongly set up config.");
									getLogger().warning("More info on how to set it up correctly on http://goo.gl/sXdl3A");
								}
							}
						}
						
						inventories.put(name, inventory);
						getLogger().info("Loaded inventory '" + name + "'.");
					} catch (Exception e) {
						getLogger().severe("Couldn't load inventory '" + name + "'. This is because of a wrongly set up config.");
						getLogger().severe("Technical exception: " + e.getMessage());
						getLogger().severe("More info on how to set it up correctly on http://goo.gl/sXdl3A");
					}
				}
			}
		}
	}
	
	public boolean canUseCompass(Player player) {
		if (getConfig().getStringList("settings.disabledWorlds").contains(player.getWorld().getName()) && !player.hasPermission("compassnav.useanywhere")) {
			return false;
		} else if (worldGuardHandler != null && !player.hasPermission("compassnav.useanywhere")) {
			return worldGuardHandler.canUseCompassHere(player.getLocation());
		}
		return true;
	}
    
    public void openInventory(Player player, String name) {
    	if (player.hasPermission(new Permission("compassnav." + name, PermissionDefault.TRUE))) {
			if (inventories.containsKey(name)) {
				Inventory inventory = Utils.cloneInventory(player.getName(), inventories.get(name));
				sessions.put(player.getName(), name + ".");
				player.openInventory(inventory);
			} else {
				getLogger().severe("You do not have the inventory '" + name + "' set up, please set it up in the config.");
				getLogger().severe("More info on how to set it up on http://goo.gl/sXdl3A");
			}
    	}
	}
    
    public void checkUsage(Player player, String inventory, int slot) {
    	if (canUseCompass(player)) {
    		checkPlayers(player, inventory, slot);
    	} else {
    		player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.cantUseMessage")));
    	}
    }
    
    public void checkPlayers(Player player, String inventory, int slot) {
    	if (timers.containsKey(player.getName())) {
    		timers.get(player.getName()).cancel();
    	}
    	
    	if (getConfig().getBoolean("settings.warmUp") && !player.hasPermission("compassnav.nowarmup") && !getConfig().contains("settings." + inventory + slot + ".inventory")) {
    		boolean delay = false;
    		
	    	for (Player p : player.getWorld().getPlayers()) {
	    		if (p.getName() != player.getName() && p.getLocation().distance(player.getLocation()) < getConfig().getInt("settings.warmUpDistance")) {
			    	delay = true;
			    	break;
	    		}
	    	}
	    	
	    	if (delay) {
	    		timers.put(player.getName(), new WarmupTimer(this, player, inventory, slot));
	    		timers.get(player.getName()).runTaskLater(this, 20L * getConfig().getInt("settings.warmUpTime"));
	    		player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.warmUpMessage").replace("<time>", Integer.toString(getConfig().getInt("settings.warmUpTime")))));
	    	} else {
	    		checkMoney(player, inventory, slot);
	    	}
    	} else {
    		checkMoney(player, inventory, slot);
    	}
    }
    
    public void checkMoney(Player player, String inventory, int slot) {
    	if (!player.hasPermission("compassnav.free") && hasEconomy()) {
	    	if (getConfig().contains("settings." + inventory + slot + ".price")) {
	    		double price = getConfig().getDouble("settings." + inventory + slot + ".price");
	    		if (vaultHandler.getEconomy().has(player.getName(), price)) {
	    			vaultHandler.getEconomy().withdrawPlayer(player.getName(), price);
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.chargedMessage").replace("<price>", Double.toString(price))));
	    			executeCommands(player, inventory, slot);
	    		} else {
	    			player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.notEnoughMessage")));
	    			player.closeInventory();
	    		}
	    	} else {
	    		executeCommands(player, inventory, slot);
	    	}
    	} else {
    		executeCommands(player, inventory, slot);
    	}
    }
    
    public void executeCommands(Player player, String inventory, int slot) {
    	for (String command : getConfig().getStringList("settings." + inventory + slot + ".commands")) {
    		if (command.startsWith("c:")) {
    			getServer().dispatchCommand(getServer().getConsoleSender(), Utils.replaceModifiers(player, command.substring(2)));
    		} else {
    			getServer().dispatchCommand(player, Utils.replaceModifiers(player, command));
    		}
    	}
    	
    	checkMessages(player, inventory, slot);
    }
    
    public void checkMessages(Player player, String inventory, int slot) {
    	for (String message : getConfig().getStringList("settings." + inventory + slot + ".messages")) {
    		player.sendMessage(Utils.replaceUnicode(ChatColor.translateAlternateColorCodes('&', message.replace("<name>", player.getName()).replace("<displayname>", player.getDisplayName()))));
    	}
    	
    	checkInventory(player, inventory, slot);
    }
    
    public void checkInventory(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".inventory")) {
    		player.closeInventory();
    		openInventory(player, getConfig().getString("settings." + inventory + slot + ".inventory"));
    	} else {
    		checkBungee(player, inventory, slot);
    	}
    }
    
    public void checkBungee(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".bungee")) {
    		try {
				ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
				DataOutputStream output = new DataOutputStream(byteOutput);
				output.writeUTF("Connect");
				output.writeUTF(getConfig().getString("settings." + inventory + slot + ".bungee"));

				player.closeInventory();
				player.sendPluginMessage(this, "BungeeCord", byteOutput.toByteArray());
			} catch (Exception e) {
				checkLilypad(player, inventory, slot);
			}
    	} else {
    		checkLilypad(player, inventory, slot);
    	}
    }
    
    public void checkLilypad(Player player, String inventory, int slot) {
    	if (hasConnect()) {
	    	if (getConfig().contains("settings." + inventory + slot + ".lilypad")) {
	    		player.closeInventory();
	    		if (!lilypadHandler.connect(player.getName(), getConfig().getString("settings." + inventory + slot + ".lilypad"))) {
	    			checkEssentials(player, inventory, slot);
	    		}
	    	} else {
	    		checkEssentials(player, inventory, slot);
	    	}
    	} else {
    		checkEssentials(player, inventory, slot);
    	}
    }
    
    public void checkEssentials(Player player, String inventory, int slot) {
    	if (haveWarps()) {
    		if (getConfig().contains("settings." + inventory + slot + ".warp")) {
    			Location location = essentialsHandler.getWarp(getConfig().getString("settings." + inventory + slot + ".warp"));
    			if (location != null) {
    				player.teleport(location);
    				player.closeInventory();
    			} else {
    				checkLocation(player, inventory, slot);
    			}
    		} else {
    			checkLocation(player, inventory, slot);
    		}
    	} else {
    		checkLocation(player, inventory, slot);
    	}
    }
    
    public void checkLocation(Player player, String inventory, int slot) {
    	if (getConfig().contains("settings." + inventory + slot + ".world") && getConfig().contains("settings." + inventory + slot + ".x") && getConfig().contains("settings." + inventory + slot + ".y") && getConfig().contains("settings." + inventory + slot + ".z") && getConfig().contains("settings." + inventory + slot + ".yaw") && getConfig().contains("settings." + inventory + slot + ".pitch")) {
    		player.closeInventory();
    		player.teleport(new Location(getServer().getWorld(getConfig().getString("settings." + inventory + slot + ".world")), (float) getConfig().getDouble("settings." + inventory + slot + ".x"), (float) getConfig().getDouble("settings." + inventory + slot + ".y"), (float) getConfig().getDouble("settings." + inventory + slot + ".z"), (float) getConfig().getDouble("settings." + inventory + slot + ".yaw"), (float) getConfig().getDouble("settings." + inventory + slot + ".pitch")));
    	} else {
    		player.closeInventory();
    	}
    }
    
    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {
    	Player player = event.getPlayer();
    	if (event.hasItem()) {
    		if (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	    		ItemStack item = event.getItem();
	    		if (getConfig().contains("settings.items." + item.getTypeId())) {
	    			openInventory(player, getConfig().getString("settings.items." + item.getTypeId()));
	    			event.setCancelled(true);
	    			return;
	    		} else if (getConfig().contains("settings.items." + item.getTypeId() + ":" + item.getDurability())) {
	    			openInventory(player, getConfig().getString("settings.items." + item.getTypeId() + ":" + item.getDurability()));
	    			event.setCancelled(true);
	    			return;
	    		}
    		}
    	}
    	
    	if (event.hasBlock()) {
    		if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
    			Block block = event.getClickedBlock();
    			if (block.getState() instanceof Sign) {
    				Sign sign = (Sign) block.getState();
    				if (sign.getLine(0).equals(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle")))) {
    					event.setCancelled(true);
    					openInventory(player, sign.getLine(2));
    				}
    			}
    		}
    	}
    }
    
    @EventHandler
    public void onCommandPreprocess(PlayerCommandPreprocessEvent event) {
    	Player player = event.getPlayer();
    	String[] args = event.getMessage().substring(1).toLowerCase().split(" ");
    	if (args.length > 0) {
			String command = args[0];
			if (getConfig().contains("settings.commands." + command)) {
				event.setCancelled(true);
				String inventory = getConfig().getString("settings.commands." + command);
				if (args.length == 1) {
					openInventory(player, inventory);
				} else if (player.hasPermission("compassnav.opensomeone")) {
					Player target = getServer().getPlayer(args[1]);
					if (target != null) {
						openInventory(target, inventory);
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.openedForMessage").replace("<player>", target.getName()).replace("<inventory>", inventory)));
					} else {
						player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.notOnlineMessage").replace("<player>", args[1])));
					}
				} else {
					openInventory(player, inventory);
				}
			}
    	}
	}
    
    @EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			Player player = (Player) event.getWhoClicked();
			if (sessions.containsKey(player.getName())) {
				event.setCancelled(true);
				String inventory = sessions.get(player.getName());
				int slot = event.getRawSlot() + 1;
				if (getConfig().contains("settings." + inventory + slot)) {
					if (player.hasPermission(new Permission("compassnav." + inventory + slot, PermissionDefault.TRUE))) {
						if (getConfig().getBoolean("settings.sounds")) {
							player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("settings.teleportSound").toUpperCase()), 1.0F, 1.0F);
						}
						checkUsage(player, inventory, slot);
					} else if (getConfig().getBoolean("settings.sounds")) {
						player.playSound(player.getLocation(), Sound.valueOf(getConfig().getString("settings.noPermSound").toUpperCase()), 1.0F, 1.0F);
					}
				}
			}
		}
	}
    
    @EventHandler
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		if (timers.containsKey(player.getName())) {
			if (event.getTo().getX() != event.getFrom().getX() || event.getTo().getY() != event.getFrom().getY() || event.getTo().getZ() != event.getFrom().getZ()) {
				timers.get(player.getName()).cancel();
				timers.remove(player.getName());
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.teleportCancelMessage")));
			}
		}
	}
    
    @EventHandler
    public void onSignChange(SignChangeEvent event) {
    	Player player = event.getPlayer();
    	if (event.getLine(0).equalsIgnoreCase(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))) || event.getLine(0).equalsIgnoreCase(ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))))) {
	    	if (player.hasPermission("compassnav.createsign")) {
	    		event.setLine(0, ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle")));
	    	} else {
	    		event.setLine(0, ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', getConfig().getString("settings.signTitle"))));
	    	}
    	}
    }
    
    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent event) {
    	Player player = event.getPlayer();
    	if (timers.containsKey(player.getName())) {
			timers.get(player.getName()).cancel();
			timers.remove(player.getName());
		}
    	sessions.remove(player.getName());
    }
    
    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
    	sessions.remove(event.getPlayer().getName());
    }
	
	public void sendHelpMessage(CommandSender sender) {
		for (String string : getConfig().getStringList("general.commandHelpMessage")) {
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', string));
		}
	}
	
	public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args) {
  		if (cmd.getName().equalsIgnoreCase("cn") || cmd.getName().equalsIgnoreCase("compassnav")) {
  			if (args.length == 0) {
  				sendHelpMessage(sender);
  			} else if (args[0].equalsIgnoreCase("reload")) {
  				if (sender.hasPermission("compassnav.reload")) {
  					reloadConfig();
  					loadInventories();
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.reloadMessage")));
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else if (args[0].equalsIgnoreCase("update")) {
  				if (sender.hasPermission("compassnav.update")) {
  					if (autoUpdater != null) {
  						autoUpdater.download();
  						if (autoUpdater.getResult() == UpdateResult.SUCCESS) {
  							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.updateSuccessMessage")));
  						} else {
  							sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.updateFailedMessage")));
  						}
  					} else {
  						sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.cantUpdateMessage")));
  					}
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else if (args[0].equalsIgnoreCase("setup")) {
  				if (sender.hasPermission("compassnav.setup")) {
  					
  				} else {
  					sender.sendMessage(ChatColor.translateAlternateColorCodes('&', getConfig().getString("general.noPermMessage")));
  				}
  			} else {
  				sendHelpMessage(sender);
  			}
  		}
  		return true;
  	}
}