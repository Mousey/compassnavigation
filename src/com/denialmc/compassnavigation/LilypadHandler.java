package com.denialmc.compassnavigation;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.request.impl.RedirectRequest;

import org.bukkit.plugin.RegisteredServiceProvider;

public class LilypadHandler {

	private Connect connect;
	
	public LilypadHandler() {
		CompassNavigation plugin = CompassNavigation.getInstance();
        RegisteredServiceProvider<Connect> service = plugin.getServer().getServicesManager().getRegistration(Connect.class);
        if (service != null) {
        	connect = service.getProvider();
        	plugin.getLogger().info("Hooked into LilyPad-Connect for server teleportation!");
        }
	}
	
	public boolean hasConnect() {
		return connect != null;
	}
	
	public Connect getConnect() {
		return connect;
	}
	
	public boolean connect(String player, String server) {
		if (hasConnect()) {
			try {
				getConnect().request(new RedirectRequest(server, player));
				return true;
			} catch (Exception e) {
				CompassNavigation.getInstance().getLogger().warning("An error happened while teleporting a player to the '" + server + "' server: " + e.getMessage());
			}
		}
		return false;
	}
}